from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException,\
    StaleElementReferenceException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json
from configuration import Configuration
from time import sleep
from progress.bar import IncrementalBar
import threading
import urllib


class AlreadySeekingError(Exception):
    pass


class SeekFriends():
    friend_selector = '.ProfileCard'
    friendships = list()
    locker = threading.Lock()

    def __init__(self, driver_name):
        if driver_name.lower() == 'firefox':
            self.driver = webdriver.Firefox()
        elif driver_name.lower() == 'safari':
            self.driver = webdriver.Safari()
        elif driver_name.lower() == 'phantomjs':
            dcap = dict(DesiredCapabilities.PHANTOMJS)
            dcap["phantomjs.page.settings.userAgent"] = (
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 9_1_3) "
                    "AppleWebKit/602.3.12 (KHTML, like Gecko) "
                    "Version/10.0.2 Safari/602.3.12"
            )
            self.driver = webdriver.PhantomJS(
                    desired_capabilities=dcap,
                    service_args=['--load-images=no'])
        else:
            self.driver = webdriver.Chrome()

        self.is_used = False

    def start(self, user, restrictions=None):
        self.is_used = True
        wanted_url = 'https://twitter.com/{}/following'.format(user)
        try:
            self.driver.get(wanted_url)
        except (ConnectionRefusedError, urllib.error.URLError):
            print('Connection refused, can\'t search for {}'.
                  format(user))
            self.is_used = False
            return

        while self.driver.current_url != wanted_url:
            sleep(1)

        nb_friends = 0
        last_nb_friends = 0
        nb_same_nb = 0

        while (nb_same_nb <= 10 and nb_friends % 3 == 0):
            last_nb_friends = nb_friends
            found_friends = []

            try:
                found_friends = self.driver.\
                    find_elements_by_css_selector(SeekFriends.friend_selector)
            except NoSuchElementException:
                print('No friends for {}'.
                      format(user))
                self.is_used = False
                return
            except (ConnectionError, urllib.error.URLError):
                print('Connection error, end search for {}'.
                      format(user))
                break

            nb_friends = len(found_friends)

            sleep(1)

            try:
                self.driver.execute_script('window.scrollTo(0,'
                                       'document.body.scrollHeight);')
            except (ConnectionError, urllib.error.URLError):
                print('Connection error, end search for {}'.
                      format(user))
                break

            if last_nb_friends == nb_friends:
                nb_same_nb += 1
            else:
                nb_same_nb = 0

        for friend in found_friends:
            try:
                friend_name = friend.get_attribute('data-screen-name')

                if restrictions == None or friend_name in restrictions:
                    SeekFriends.locker.acquire()
                    SeekFriends.friendships.append((user, friend_name))
                    SeekFriends.locker.release()
            except StaleElementReferenceException:
                print('Lost element reference.', tweet)
            except (ConnectionError, urllib.error.URLError):
                print('Connection error, end search for {}'.
                      format(user))
                break

        self.is_used = False

    def start_thread(self, user, restrictions=None):
        if self.is_used:
            raise AlreadySeekingError

        self.is_used = True
        threading.Thread(target=self.start, args=(user,restrictions)).start()

    def quit_driver(self):
        if self.is_used:
            return False
        else:
            print("Close thread")
            self.driver.quit()
            return True


def get_users_names(input_file):
    with open(input_file) as f:
        data = json.load(f)

    ids = list()
    for u in data:
        ids.append(u["screen_name"])

    return ids


def connection(
        key,
        secret_key,
        token,
        secret_token):
    auth = tweepy.OAuthHandler(key, secret_key)
    auth.set_access_token(token, secret_token)
    api = tweepy.API(auth)
    return api
    

def get_relations(driver_name, users, restrict_to_users=True, nb_threads=1):
    seekers = list()
    for i in IncrementalBar('Start drivers', max=nb_threads).iter(range(nb_threads)):
        seekers.append(SeekFriends(driver_name))

    progress_bar = IncrementalBar('Parsing friends', max=len(users), suffix='%(index)d/%(max)d [%(elapsed)ds / %(eta)ds]')
    for u in users:
        seeking = False
        while not seeking:
            for s in seekers:
                try:
                    if restrict_to_users:
                        s.start_thread(u, users)
                    else:
                        s.start_thread(u)
                    progress_bar.next()
                    seeking = True
                    break
                except AlreadySeekingError:
                    pass
        if not seeking:
            sleep(1)
    
    progress_bar.finish()
    sleep(10) # Assure le lancement du dernier thread
    # === Fermetures ===
    progress_bar = IncrementalBar('Waiting for end', max=nb_threads)
    while len(seekers) > 0:
        for s in seekers:
            if s.quit_driver():
                progress_bar.next()
                seekers.remove(s)
        sleep(1)
    progress_bar.finish()

    print('\nResearch done, {} friends found'.
          format(len(SeekFriends.friendships)))
    
    return SeekFriends.friendships

if __name__ == "__main__":
    config = Configuration('config.ini')
    users = get_users_names(config.cleaner.input_file)

    relations = get_relations(config.cleaner.driver, users, nb_threads=config.cleaner.nb_browsers)

    with open(config.cleaner.output_file, 'w') as f:
        for r in relations:
            f.write('{} {}\n'.format(r[0], r[1]))
