import configparser
import json
from progress.bar import IncrementalBar


def get_linked_users(tweet):
    ids_users = set()
    if('in_reply_to_user_id' in tweet
       and tweet['in_reply_to_user_id'] is not None):
        ids_users.add(tweet['in_reply_to_user_id'])

    if('entities' in tweet
       and 'user_mension' in tweet['entities']):
        for user in tweet['entities']['user_mension']:
            ids_users.add(user['id'])

    return ids_users


config = configparser.ConfigParser()
config.read('config.ini')
with open(config['Relations']['input']) as input_file:
    tweets = json.load(input_file)

relations = set()
for tweet in IncrementalBar('Processing tweets').iter(tweets):
    user = tweet['user']
    linked_users = get_linked_users(tweet)
    for u in linked_users:
        relations.add((user, u))

with open(config['Relations']['output'], 'w') as f:
    for r in relations:
        f.write('{} {}\n'.format(r[0], r[1]))
