import igraph

graph = igraph.Graph.Read_Ncol('relations.txt')
#igraph.plot(graph)

communities = graph.community_edge_betweenness()
nb_communities = communities.optimal_count
communities.as_clustering(nb_communities)
