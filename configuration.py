import configparser


class Configuration:
    def __init__(self, config_file):
        config = configparser.ConfigParser()
        config.read(config_file)

        self.cleaner = ConfigurationCleaner(config["Cleaner"])
        self.twitter = ConfigurationTwitter(config["Twitter"])

    def __str__(self):
        return "dirty_file : {}\nnodes_file : {}\nedges_file : {}".\
                format(self.dirty_file, self.nodes_file, self.edges_file)


class ConfigurationTwitter:
    def __init__(self, config):
        self.key = config["key"]
        self.secret_key = config["secret-key"]
        self.token = config["token"]
        self.secret_token = config["secret-token"]


class ConfigurationCleaner:
    def __init__(self, config):
        self.input_file = config["input-file"]
        self.output_file = config["output-file"]
        self.nb_browsers = int(config["nb-browsers"])
        self.driver = config["driver"]
